
#Many of these are conditions for unqiue descriptions of the "Fleet visits X" event
JY_LF_beikdugang_lights = {
	AND = {
		province_id = 4907
		owner = { mission_completed = Y15_gateway_aelantir }
	}
}

JY_LF_beikdugang_nolights = {
	AND = {
		province_id = 4907
		owner = { NOT = { mission_completed = Y15_gateway_aelantir } }
	}
}

JY_LF_keoaden = {
	province_id = 4955
}

JY_LF_sarkhashabid = {
	province_id = 4367
}